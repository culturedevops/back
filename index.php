<!DOCTYPE html>
<html>
<head>
    <title>Page d'accueil</title>
</head>
<body>
    <h1>Bienvenue sur notre site</h1>
    
    <?php
    
    include './class/router.class.php';
    require 'vendor/autoload.php'; 
    include './controller/controllerFile.php';
    // Code PHP pour vérifier si l'utilisateur est connecté et afficher un message correspondant
    session_start();
    if($_GET['url']) {
        $router = new Router($_GET['url']); 
        $router->get('index', function(){ echo "Bienvenue sur ma homepage !"; }); 
        $router->get('/posts/:id', function($id){ echo "Voila l'article $id"; }); 
        $router->post('/file', function(){  
            
            $host = '188.165.201.9';
            $dbName = 'DevopsDB';
            $username = 'admin';
            $password = 'test';
            try {
                $pdo = new PDO("mysql:host=$host;dbname=$dbName;charset=utf8", $username, $password);
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
                // Gérer l'erreur de connexion à la base de données
                die("Erreur de connexion à la base de données : " . $e->getMessage());
            }
            $uploadDirectory = './file/';

            if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_FILES['file'])) {
                $file = $_FILES['file'];
                // Vérifie l'extension du fichier
                $allowedExtensions = ['pdf', 'jpg', 'jpeg', 'png', 'svg', 'mp4'];
                $fileExtension = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
                if (!in_array($fileExtension, $allowedExtensions)) {
                    $response = [
                        'success' => false,
                        'message' => 'Extension de fichier non autorisée.'
                    ];
                } else {
                    $title = $_POST['title'];
                    $description = $_POST['description'];
                    $folderId = $_POST['folder_id'];

                    $fileName = $file['name'];
                    $fileSize = $file['size'];
                    $fileTmpPath = $file['tmp_name'];
                    $fileUploadPath = $uploadDirectory . $fileName;
                    $createdAt = date('Y-m-d H:i:s');
                    $updateddAt = date('Y-m-d H:i:s');
            
                    if (move_uploaded_file($fileTmpPath, $fileUploadPath)) {
                          // Insérer les données dans la base de données
                        $sql = "INSERT INTO file (file_description,
                        file_title, 
                        file_name, 
                        file_extension, 
                        folder_id,
                        file_size, 
                        file_path, 
                        created_at, 
                        updated_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
                        $stmt = $pdo->prepare($sql);
                        $stmt->execute([$description, 
                        $title, 
                        $fileName, 
                        $fileExtension,
                        $folderId, 
                        $fileSize, 
                        $fileUploadPath, 
                        $createdAt, 
                        $updateddAt]);

                        $response = [
                            'success' => true,
                            'message' => 'Fichier reçu et enregistré avec succès.',
                            'data' => [
                                'title' => $title,
                                'description' => $description,
                                'fileName' => $fileName,
                                'fileExtension' => $fileExtension,
                                'fileSize' => $fileSize,
                                'filePath' => $fileUploadPath,
                                'createdAt' => $createdAt,
                                'updateddAt' => $updateddAt
                            ]
                        ];
                    } else {
                        $response = [
                            'success' => false,
                            'message' => 'Une erreur s\'est produite lors de l\'enregistrement du fichier.'
                        ];
                    }
                }
            
                // Répond avec la réponse JSON
                header('Content-Type: application/json');
                echo json_encode($response);
                exit();
            }
            
            // Si aucune requête POST avec un fichier n'a été effectuée
            $response = [
                'success' => false,
                'message' => 'Aucun fichier n\'a été envoyé.'
            ];
            
            // Répond avec la réponse JSON
            header('Content-Type: application/json');
            echo json_encode($response);
     }); 
        $router->get('/documents/:int', function($int){
            echo json_encode(FileController("GetAll",$int));
        }); 
        $router->run(); 
    }
    ?>
</body>
</html>