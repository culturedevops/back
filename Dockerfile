FROM php:8.0-apache
WORKDIR /var/www/html

RUN apt update \
      && apt search php \
      && apt install libzip-dev -y \
      && docker-php-ext-install zip

RUN docker-php-ext-install mysqli pdo pdo_mysql && docker-php-ext-enable pdo_mysql
COPY --from=composer /usr/bin/composer /usr/bin/composer
COPY . .

RUN composer install

EXPOSE 80
